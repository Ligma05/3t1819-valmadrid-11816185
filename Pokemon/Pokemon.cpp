#include "Pokemon.h"

Pokemon::Pokemon()
{
	this->pokemon = "";
	this->baseHP = 0;
	this->hp = 0;
	this->MaxHP = 0;
	this->baseDamage = 0;
	this->exp = 0;
	this->expToNextLevel = 0;

}

Pokemon::Pokemon(string pokemon, int hp, int baseHP, int MaxHP, int baseDamage, int exp, int expToNextLevel)
{

	this->pokemon = pokemon;
	this->hp = hp;
	this->baseHP = baseHP;
	this->MaxHP = MaxHP;
	this->baseDamage = baseDamage;
	this->exp = exp;
	this->expToNextLevel = expToNextLevel;

}

void Pokemon::pokemonRandomizer()
{
	Pokemon* pokemon[12] = { new Pokemon("Aron", 210, 210, 210, 130, 4, 1), new Pokemon("Psyduck", 210, 210, 210, 98, 3, 1), new Pokemon("Blastoise", 268, 268, 268, 153, 6, 1), new Pokemon("Psyduck", 210, 210, 210, 98, 4, 1), new Pokemon("Blaziken", 270, 270, 270, 220, 6, 1), new Pokemon("Kyogre", 310, 310, 310, 184, 8, 1), new Pokemon("Darkrai", 250, 250, 250, 166, 7, 1), new Pokemon("Dialga", 310, 310, 310, 220, 9, 1), new Pokemon("Gengar", 250, 250, 250, 150, 4, 1), new Pokemon("Magikarp", 280, 280, 280, 250, 9, 0), new Pokemon("Groudon", 310, 310, 310, 274, 3, 1), new Pokemon("Mew Two", 322, 322, 322, 202, 10, 0) };

	int pokemonRandom = rand() % 12;
	this->currentPokemon = pokemon[pokemonRandom];
}

void Pokemon::pokemonStats()
{
	cout << "\nPokemon: " << this->pokemon << " stats:\n";
	cout << "Max HP: " << this->MaxHP << "stats:\n";
	cout << "Heatlh Points: " << this->hp << endl;
	cout << "Base Damage: " << this->baseDamage << endl;
	cout << "Exp: " << this->exp << endl;
}

void Pokemon::healingProcess()
{
	this->hp = this->MaxHP;
}
