#pragma once
#include <iostream>
#include <string>

using namespace std;

class Pokemon
{
public:
	Pokemon();
	Pokemon(string pokemon, int hp, int baseHP, int MaxHP, int baseDamage, int exp, int expToNextLevel);
	string pokemon;
	int hp;
	int baseHP;
	int MaxHP;
	int baseDamage;
	int exp;
	int expToNextLevel;
	Pokemon *currentPokemon;
	void pokemonRandomizer();
	void pokemonStats();
	void healingProcess();
};