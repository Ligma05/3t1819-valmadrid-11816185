#include <iostream>
#include <string>
#include <vector>
#include "Pokemon.h"
#include "Trainer.h"
#include <time.h>


using namespace std;


int main() {
	srand(time(NULL));
	Trainer* myTrainer = new Trainer();
	Pokemon* myPokemon = new Pokemon();
	myTrainer->playerInput();
	myTrainer->starterPokemon();
	
	myTrainer->mainAction(myPokemon);

}