#include "Trainer.h"
Trainer::Trainer()
{
	this->name = "";
	this->x = 0;
	this->y = 0;
}

Trainer::Trainer(string name, int x, int y)
{
	this->name = name;
	this->x = x;
	this->y = y;
}

void Trainer::trainerLocation()
{
	cout << "\nTrainer: " << this->name << endl;
	cout << "Location: " << "(" << this->x << "," << this->y << ")";
}

void Trainer::displaystats(Pokemon * pokemon)
{
	cout << "\nPokemon: " << pokemon->pokemon << " stats:\n";
	cout << "Max Health Points: " << pokemon->MaxHP << endl;
	cout << "Heatlh Points: " << pokemon->hp << endl;
	cout << "Base Damage: " << pokemon->baseDamage << endl;
	cout << "Exp: " << pokemon->exp << endl;
	cout << "Exp to the next level: " << pokemon->expToNextLevel << endl;

}

void Trainer::playerInput()
{
	cout << "Hi im Professor Oak" << endl;
	system("Pause");

	cout << "People refer to me as the Professor of Pokemons" << endl;
	cout << "What is your name? " << endl;
	cin >> name;
	Trainer*myPlayer = new Trainer(this->name, this->x, this->y);
	cout << endl << "Welcome " << name << " To the World of Pokemon!" << endl;
	system("pause");
	system("CLS");
	
}

void Trainer::starterPokemon()
{
	Pokemon* StarterPokemon1 = new Pokemon("Pikachu", 180, 180, 180, 103, 1, 1);
	Pokemon* StarterPokemon2 = new Pokemon("Squirtle", 198, 198, 198, 90, 1, 1);
	Pokemon* StarterPokemon3 = new Pokemon("Charmander", 188, 188, 188, 98, 1, 1);
	
	cout << "Choose your Pokemon Starter: " << endl;
	cout << "A = Pikachu " << endl;
	cout << "B = Squirtle " << endl;
	cout << "C = Charmander " << endl;
	cin >> choice;
	if (choice == 'A' || choice == 'a') {
		this->pokemonList.push_back(StarterPokemon1);
		cout << "you chose Pikachu" << endl;
	}
	else if (choice == 'B' || choice == 'b') {
		this->pokemonList.push_back(StarterPokemon2);
		cout << "you chose Squirtle" << endl;
	}
	else if (choice == 'C' || choice == 'c') {
		this->pokemonList.push_back(StarterPokemon3);
		cout << "you chose Charmander" << endl;
	}
	
	system("pause");
}

void Trainer::mainAction(Pokemon* pokemon)
{
	while (pokemonList.size() != 0) {
		if (this->x <= 2 || this->x >= -2 || this->y <= 2 || this->y >= -2) {
			
			cout << "What do you want to do: " << endl;
			cout << "[A] Move" << endl;
			cout << "[B] Display stats of all pokemon" << endl;
			cout << "[C] Go to Pokemon Center" << endl;
			cin >> choice;
			if (choice == 'A' || choice == 'a') {
				cout << "Choose a Direction: " << endl;
				cout << "[A] North\n";
				cout << "[B] South\n";
				cout << "[C] East\n";
				cout << "[D] West\n";
				cin >> choice;
				if (choice == 'A' || choice == 'a') {
					this->y++;
				}
				else if (choice == 'B' || choice == 'b') {
					this->y--;
				}
				else if (choice == 'C' || choice == 'c') {
					this->x++;
				}
				else if (choice == 'D' || choice == 'd') {
					this->x--;
				}
				this->trainerLocation();
				system("pause");
				system("CLS");
			}
			else if (choice == 'B' || choice == 'b') {
				for (unsigned int i = 0; i < pokemonList.size(); i++) {
					this->displaystats(pokemonList[i]);
				}
			}
			else if (choice == 'C' || choice == 'c') {
				this->pokemonCenter();
			}
			
		}
		if (this->x > 2 || this->x < -2 || this->y > 2 || this->y < -2) {
			cout << "Currently in Wild Area\n";
			system("pause");
			this->pokemonEncounter(pokemon);
		}
	}
}

void Trainer::pokemonEncounter(Pokemon*pokemon)
{
	int pokemonEncounter = rand() % 100 + 1;
	if (pokemonEncounter <= 50) {
		cout << "\nyou have encounter:\n";
		pokemon->pokemonRandomizer();
		cout << pokemon->currentPokemon->pokemon << endl;
		cout << "Choose your faith:\n";
		cout << "[A] Battle\n";
		cout << "[B] Catch\n";
		cout << "[C] Flee\n";
		cin >> choice;
		if (choice == 'A' || choice == 'a') {
			cout << "Enemy:\n";
			cout << pokemon->currentPokemon->pokemon << endl;
			cout << "Choose a Pokemon:\n";
			int counter = 1;

			for (unsigned int i = 0; i < pokemonList.size(); i++) {
				cout << counter << endl;
				this->displaystats(pokemonList[i]);
				counter++;
			}
			cin >> pick;
			
			if (pokemonList[pick-1]->hp != 0) {
				cout << "\nYou chose:\n";
				this->displaystats(pokemonList[pick - 1]);
				cout << "as your Fighter\n" << endl;
			    
				cout << "Current Enemy: " << pokemon->currentPokemon->pokemon << endl;
				cout << "Current pokemon:" << pokemonList[pick - 1]->pokemon << endl;
				while (pokemonList[pick - 1]->hp != 0||pokemon->currentPokemon->hp!=0) {
					
					cout << pokemonList[pick - 1]->pokemon << "Attacked\n";
					this->damageHp(pokemon);
					cout << pokemon->currentPokemon->pokemon << "'s Hp:\n";
					cout << pokemon->currentPokemon->hp << endl;
					system("pause");
					cout << pokemon->currentPokemon->pokemon << "Attack\n";
					this->playerDamageHp(pokemon);
					cout << pokemonList[pick - 1]->pokemon << "'s Hp:\n";
					cout << pokemonList[pick-1]->hp << endl;
					system("pause");
				
				}
				if (pokemonList[pick-1]->hp <= 0) {
					cout << "Your pokemon Lost\n";
					
				}
				else if (pokemon->currentPokemon->hp <= 0) {
					cout << "You won the match\n";
					this->chancesGainingexp(pokemon);
				}
			}
			else if (pokemonList[pick-1]->hp == 0) {
				cout << "Pokemon is dead move on already!\n";
			}
		}
		else if (choice == 'B' || choice == 'b') {
			cout << "you chose to capture " << pokemon->currentPokemon->pokemon << endl;
			cout << "Capturing pokemon.........\n";
			this->captureSequence(pokemon);
		}
		else {
			cout << "You have chosen to be a coward and decided to flee like the dog you are\n";
		}
	}
	else {
		//proceed
		cout << "no pokemons encountered proceed\n";
	}
}

void Trainer::captureSequence(Pokemon*pokemon)
{
	int pokeBall = rand() % 100 + 1;
	if (pokeBall <= 30) {
		cout << "Capture Successful";
		pokemonList.push_back(pokemon->currentPokemon);
	}
	else {
		cout << "The pokemon is too strong to be captured\n";
		cout << "you were blown away by " << pokemon->currentPokemon->pokemon << endl;
		
	}
}

void Trainer::pokemonCenter()
{
	if (this->x <= 2 && this->x >= -2 && this->y <= 2 && this->y >= -2) {
		cout << "you are in a Safe Location:\n";
		cout << "Do you want to go to the pokemon center to heal your pokemon:\n";
		cout << "[A] Yes\n";
		cout << "[B] No\n";
		cin >> choice;
		if (choice == 'A' || choice == 'a') {
			cout << "processing..... healing all your pokemon" << endl;
			//call function of healing ur pokemon
			for (unsigned int i = 0; i < pokemonList.size(); i++) {
				this->pokemonList[0]->healingProcess();
			}
			cout << "Healed all Pokemons\n";
		}
		else {


			// back to trainerChoice()
		}
	}
	else {
		cout << "you are out of the safezone" << endl;
		cout << "cannot proceed to Pokemon center\n";



	}
}


void Trainer::damageHp(Pokemon * pokemon)
{
	pokemon->currentPokemon->hp -= pokemonList[pick - 1]->baseDamage;
}

void Trainer::playerDamageHp(Pokemon * pokemon)
{
	pokemonList[pick - 1]->hp-= pokemon->currentPokemon->baseDamage;
}

void Trainer::chancesGainingexp(Pokemon * pokemon)
{
	int pokemonLevelUp = rand() % 100 + 1;
	if (pokemonLevelUp <= 50) {
		this->winningExp(pokemon);
	}
	else {

	}
}

void Trainer::winningExp(Pokemon * pokemon)
{
	pokemonList[pick - 1]->exp = +pokemonList[pick - 1]->expToNextLevel;
	pokemonList[pick - 1]->MaxHP = pokemonList[pick - 1]->MaxHP * pokemonList[pick - 1]->exp * 0.15;
	pokemonList[pick - 1]->hp = pokemonList[pick - 1]->hp * pokemonList[pick - 1]->exp * 0.15;
}
