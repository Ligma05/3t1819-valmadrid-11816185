#pragma once
#include <iostream>
#include <string>
#include "Pokemon.h"
#include <vector>

using namespace std;

class Pokemon;
class Trainer
{
public:
	Trainer();
	Trainer(string name, int x, int y);
	string name;
	int x;
	int y;
	char choice;
	int pick;
	vector<Pokemon*> pokemonList;
	void trainerLocation();
	void displaystats(Pokemon*pokemon);
	void playerInput();
	void starterPokemon();
	void mainAction(Pokemon*pokemon);
	void pokemonEncounter(Pokemon*pokemon);
	void captureSequence(Pokemon* pokemon);
	void pokemonCenter();
	void damageHp(Pokemon* pokemon); //player attacks enemy
	void playerDamageHp(Pokemon* pokemon); //enemy attacks player
	void chancesGainingexp(Pokemon*pokemon);
	void winningExp(Pokemon*pokemon);

	//format: vector<DataType> nameOfVector
	//soldiers.push_back(value) ==> adds an element to the end of the vector (also resizes it)
	//soldiers.at (index) ==> returns element at specified index number
	//soldiers.size()==> returns an unsigned int equal top the number of elements
	//soldiers.begin()==> reads vector from first element(index 0)
	//soldiers.insert(soldier.begin() + integer, new value) ==> adds element before specified index number
	//soldiers.erase(soldier.begin() + integer) ==> removes element before specified index number
	//soldiers.clear()==> removes all elements in vector
	//soldiers.empty()==> returns boolean value if whether
};